package ir.negarrajabi.config

/*
  Created by Negar on 2021/07.
 */

class AppConfig {
    companion object{
        const val SPLASH_TIME = 300L //milliseconds
        const val IMAGE_MAX_SIZE = 2048 //kb
    }
}