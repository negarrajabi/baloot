package ir.negarrajabi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ir.negarrajabi.R
import ir.negarrajabi.databinding.RowArticleBinding
import ir.negarrajabi.databinding.RowNewsBinding
import ir.negarrajabi.viewModel.HomeViewModel


/*
  Created by Negar on 2021/07.
 */


class NewsAdapter(val context: Context, val dataList: ArrayList<HomeViewModel>) :
    RecyclerView.Adapter<NewsAdapter.ProductViewHolder>() {


    class ProductViewHolder(var binding: RowArticleBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NewsAdapter.ProductViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<RowArticleBinding>(
            inflater,
            R.layout.row_article,
            parent,
            false
        )
        return ProductViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: NewsAdapter.ProductViewHolder, position: Int) {
        val viewModel = dataList.get(position)
//        val listener = MainShopActivity.Listener();
        val binder = holder.binding;
        binder.viewModel = viewModel;

    }
}