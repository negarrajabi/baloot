package ir.negarrajabi.adapter

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso
import ir.negarrajabi.R


/*
  Created by Negar on 2021/07.
 */
class BindingAdapter {
    companion object {
        @BindingAdapter("android:loadImageByUrl")
        @JvmStatic
        fun loadImage(imageView: ImageView, path: String?) {

            if (!path.isNullOrEmpty())
                Picasso.with(imageView.context).load(path).error(R.drawable.placeholder).placeholder(R.drawable.placeholder).into(imageView)
            else {
                Picasso.with(imageView.context).load(R.drawable.placeholder).error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder).into(imageView)



            }


        }
    }
}