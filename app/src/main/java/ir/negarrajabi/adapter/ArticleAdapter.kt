package ir.negarrajabi.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.negarrajabi.R
import ir.negarrajabi.local.Article
import kotlinx.android.synthetic.main.row_article.view.*


/*
  Created by Negar on 2021/07.
 */


class ArticleAdapter(
    private val users: ArrayList<Article>
) : RecyclerView.Adapter<ArticleAdapter.DataViewHolder>() {

    class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(user: Article) {
            itemView.txtName.text = user.title

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DataViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.row_article, parent,
                false
            )
        )

    override fun getItemCount(): Int = users.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) =
        holder.bind(users[position])

    fun addData(list: List<Article>) {
        users.addAll(list)
    }

}