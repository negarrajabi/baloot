package ir.negarrajabi.model

/*
  Created by Negar on 2021/07.
 */

class ErrorApi(var code: Int) {

    companion object {
        const val ON_FAILURE_ERROR = 2000
        const val NOT_SUCCESS = 2001
    }

    var message = "Error , Not Success!"

    constructor(code: Int, message: String) : this(code) {
        this.message = message;
    }

}