package ir.negarrajabi.model
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


/*
  Created by Negar on 2021/07.
 */

data class NewsResponse(
    @SerializedName("articles")
    val articles: ArrayList<Article>,
    @SerializedName("status")
    val status: String,
    @SerializedName("totalResults")
    val totalResults: Int
) {

    data class Article(
        @SerializedName("author")
        val author: String?,
        @SerializedName("content")
        val content: String?,
        @SerializedName("description")
        val description: String?,
        @SerializedName("publishedAt")
        val publishedAt: String?,
        @SerializedName("source")
        val source: Source,
        @SerializedName("title")
        val title: String?,
        @SerializedName("url")
        val url: String?,
        @SerializedName("urlToImage")
        val urlToImage: String?
    ):Parcelable {
        constructor(parcel: Parcel) : this(
            TODO("author"),
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readString()!!,
            TODO("source"),
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readString()!!
        ) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(content)
            parcel.writeString(description)
            parcel.writeString(publishedAt)
            parcel.writeString(title)
            parcel.writeString(url)
            parcel.writeString(urlToImage)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<Article> {
            override fun createFromParcel(parcel: Parcel): Article {
                return Article(parcel)
            }

            override fun newArray(size: Int): Array<Article?> {
                return arrayOfNulls(size)
            }
        }
    }

    data class Source(
        @SerializedName("id")
        val id: Any,
        @SerializedName("name")
        val name: String
    )
}