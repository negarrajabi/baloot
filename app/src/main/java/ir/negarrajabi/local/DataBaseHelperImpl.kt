package ir.negarrajabi.local

/*
  Created by Negar on 2021/07.
 */

class DatabaseHelperImpl(private val appDatabase: ArticleDataBase) : DatabaseHelper {

    override suspend fun getUsers(): List<Article> = appDatabase.articleDao().getAll()

    override suspend fun insertAll(users: List<Article>) = appDatabase.articleDao().insertAll(users)

    override suspend fun deleteAll() = appDatabase.articleDao().delete()

}