package ir.negarrajabi.local

import androidx.room.Database
import androidx.room.RoomDatabase

/*
  Created by Negar on 2021/07.
 */

@Database(entities = [Article::class], version = 1)
abstract class ArticleDataBase : RoomDatabase() {

    abstract fun articleDao(): ArticleDao

}