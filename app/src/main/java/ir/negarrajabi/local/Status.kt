package ir.negarrajabi.local

/*
  Created by Negar on 2021/07.
 */

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}