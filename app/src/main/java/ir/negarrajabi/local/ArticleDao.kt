package ir.negarrajabi.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

/*
  Created by Negar on 2021/07.
 */

@Dao
interface ArticleDao {

    @Query("SELECT * FROM articles")
    suspend fun getAll(): List<Article>

    @Insert
    suspend fun insertAll(users: List<Article>)

    @Query("DELETE FROM articles")
    suspend fun delete()

}