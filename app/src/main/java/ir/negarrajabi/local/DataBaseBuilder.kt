package ir.negarrajabi.local

import android.content.Context
import androidx.room.Room

/*
  Created by Negar on 2021/07.
 */

object DatabaseBuilder {

    private var INSTANCE: ArticleDataBase? = null

    fun getInstance(context: Context): ArticleDataBase {
        if (INSTANCE == null) {
            synchronized(ArticleDataBase::class) {
                INSTANCE = buildRoomDB(context)
            }
        }
        return INSTANCE!!
    }

    private fun buildRoomDB(context: Context) =
        Room.databaseBuilder(
            context.applicationContext,
            ArticleDataBase::class.java,
            "articles"
        ).build()

}
