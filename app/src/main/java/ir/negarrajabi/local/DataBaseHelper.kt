package ir.negarrajabi.local

/*
  Created by Negar on 2021/07.
 */

interface DatabaseHelper {

    suspend fun getUsers(): List<Article>

    suspend fun insertAll(users: List<Article>)

    suspend fun deleteAll()


}