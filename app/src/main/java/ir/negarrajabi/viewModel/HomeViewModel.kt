package ir.negarrajabi.viewModel

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.databinding.Bindable
import androidx.databinding.BindingAdapter
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.*
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ir.negarrajabi.R
import ir.negarrajabi.adapter.NewsAdapter
import ir.negarrajabi.adapter.RequestListAdapter
import ir.negarrajabi.config.preference.PrefKey
import ir.negarrajabi.local.*
import ir.negarrajabi.model.NewsResponse
import ir.negarrajabi.remote.repository.NewsRepository
import ir.negarrajabi.tools.EndlessRecyclerViewScrollListener
import kotlinx.coroutines.launch


/*
  Created by Negar on 2021/07.
 */


class HomeViewModel() : BaseViewModel() {

    lateinit var context: Context;
    lateinit var viewGroup: ViewGroup
    private val users = MutableLiveData<Resource<List<Article>>>()
    var articleT = ArrayList<HomeViewModel>()
    lateinit var dbHelper: DatabaseHelper


    constructor(context: Context, viewGroup: ViewGroup, dbHelper: DatabaseHelper) : this() {
        this.context = context
        this.viewGroup = viewGroup
        this.dbHelper = dbHelper
        orderList = ArrayList<HomeViewModel>()
        adapter = RequestListAdapter(context, orderList)
        adapter2 = NewsAdapter(context, articleT)

        getData(1)

    }

    constructor(request: NewsResponse.Article?) : this() {
        this.request = request
    }

    constructor(articles: Article) : this() {
        this.articles = articles
    }

    fun getData(page: Int) {
        val repo = NewsRepository(context)
        repo.getNews(page)
        repo.newsLiveData.observe(context as LifecycleOwner, Observer {
            cacheData = false
            orderList.addAll(it)
            adapter!!.notifyDataSetChanged()
            if(page == 1) {
                viewModelScope.launch {
                    try {
                        dbHelper.deleteAll()
                        val usersToInsertInDB = mutableListOf<Article>()
                        orderList.forEach {
                            val article = Article(
                                it.request!!.author,
                                it.request!!.title,
                                it.request!!.description!!,
                                it.request!!.url,
                                it.request!!.urlToImage,
                                it.request!!.publishedAt,
                                it.request!!.content
                            )
                            usersToInsertInDB.add(article)
                            articleT.add(HomeViewModel(article))

                        }
                        dbHelper.insertAll(usersToInsertInDB)

                        users.postValue(Resource.success(usersToInsertInDB))

                } catch (e: Exception) {
                    users.postValue(Resource.error("Something Went Wrong", null))
                }

                }

            }


        })

        repo.errorLiveData.observe(context as LifecycleOwner, Observer {
            cacheData = true
                Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
            viewModelScope.launch {
                users.postValue(Resource.loading(null))
                    val usersFromDb = dbHelper.getUsers()
                        users.postValue(Resource.success(usersFromDb))
                        usersFromDb.forEach {
                            articleT.add(HomeViewModel(it))
                        }

            }
        })

    }




    companion object {

        @JvmStatic
        @BindingAdapter("app:initRecycler")
        public fun recyclerRequest(
            recyclerView: RecyclerView,
            viewModel: HomeViewModel
        ) {

            val layoutManager =
                LinearLayoutManager(recyclerView.context, RecyclerView.VERTICAL, false)
            recyclerView.layoutManager = layoutManager
            recyclerView.adapter = viewModel.adapter
            recyclerView.addOnScrollListener(object :
                EndlessRecyclerViewScrollListener(layoutManager) {
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                    viewModel.getData(page+1)
                }
            })
        }

        @JvmStatic
        @BindingAdapter("app:articleRec")
        public fun recyclerArticle(
            recyclerView: RecyclerView,
            viewModel: HomeViewModel
        ) {

            val layoutManager =
                LinearLayoutManager(recyclerView.context, RecyclerView.VERTICAL, false)
            recyclerView.layoutManager = layoutManager
            recyclerView.adapter = viewModel.adapter2
        }

    }

    fun getUsers(): LiveData<Resource<List<Article>>> {
        return users
    }

    fun openNews(view:View, viewModel: HomeViewModel,cache : Boolean){
        val bundle = Bundle()
        if(cache)
            bundle.putParcelable(PrefKey.ARTICLE,viewModel.articles)
        else
            bundle.putParcelable(PrefKey.NEWS,viewModel.request)
        bundle.putBoolean(PrefKey.TYPE,cache)
        view.findNavController().navigate(R.id.newsDetailFragment,bundle)
    }

    var orderList = ArrayList<HomeViewModel>()
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.orderList)
        }

    var adapter: RequestListAdapter? = null
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.adapter)
        }

    var adapter2: NewsAdapter? = null
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.adapter2)
        }

    var request: NewsResponse.Article? = null
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.request)
        }
    var articles: Article? = null
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.articles)
        }

    var cacheData: Boolean = false
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.cacheData)
        }


}



