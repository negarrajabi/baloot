package ir.negarrajabi.viewModel

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.*
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import ir.negarrajabi.config.AppData


/*
  Created by Negar on 2021/07.
 */

class ProfileViewModel() : BaseViewModel() {

    lateinit var context: Context;
    lateinit var viewGroup: ViewGroup
    lateinit var bottomSheet: BottomSheetBehavior<ConstraintLayout>


    constructor(context: Context, viewGroup: ViewGroup) : this() {
        this.context = context
        this.viewGroup = viewGroup
    }

    fun openLinkedin(view:View){
        val browserIntent =
            Intent(Intent.ACTION_VIEW, Uri.parse(AppData.LINKEDIN_LINK))
        view.context.startActivity(browserIntent)
    }
    fun openGit(view:View){
        val browserIntent =
            Intent(Intent.ACTION_VIEW, Uri.parse(AppData.GIT_LINK))
        view.context.startActivity(browserIntent)
    }
 }



