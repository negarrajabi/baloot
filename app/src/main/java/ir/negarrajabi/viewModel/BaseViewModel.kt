package ir.negarrajabi.viewModel

import android.graphics.Typeface
import android.view.View
import android.widget.TextView
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import com.google.android.material.snackbar.Snackbar
import ir.negarrajabi.R

/*
  Created by Negar on 2021/07.
 */

open class BaseViewModel : ViewModel(), Observable {

    protected fun errorSnack(view: View, message :String): Snackbar {
        val snackbar = Snackbar.make(view,message, Snackbar.LENGTH_LONG)
        val tv = snackbar.view.findViewById<TextView>(R.id.snackbar_text)
        val ac = snackbar.view.findViewById<TextView>(R.id.snackbar_action)
        val typeface = Typeface.createFromAsset(view.context.assets, "fonts/IRANSans(FaNum).ttf")
        tv.typeface = typeface
        ac.typeface = typeface
        return snackbar

    }
    private val callbacks = PropertyChangeRegistry()

    override fun addOnPropertyChangedCallback(
        callback: Observable.OnPropertyChangedCallback
    ) {
        callbacks.add(callback)
    }

    override fun removeOnPropertyChangedCallback(
        callback: Observable.OnPropertyChangedCallback
    ) {
        callbacks.remove(callback)
    }

    /**
     * Notifies observers that all properties of this instance have changed.
     */
    fun notifyChange() {
        callbacks.notifyCallbacks(this, 0, null)
    }

    /**
     * Notifies observers that a specific property has changed. The getter for the
     * property that changes should be marked with the @Bindable annotation to
     * generate a field in the BR class to be used as the fieldId parameter.
     *
     * @param fieldId The generated BR id for the Bindable field.
     */
    fun notifyPropertyChanged(fieldId: Int) {
        callbacks.notifyCallbacks(this, fieldId, null)
    }
fun back(view:View){
    view.findNavController().navigateUp()
}

    var toolbarTitle = ""
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.toolbarTitle)
        }
}