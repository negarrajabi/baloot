package ir.negarrajabi.viewModel.factory

import android.content.Context
import android.view.ViewGroup
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ir.negarrajabi.local.DatabaseHelper

/*
  Created by Negar on 2021/07.
 */

class MyViewModelFactory() {

    inner class BaseViewModelFactory(val context: Context, val viewGroup: ViewGroup) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return modelClass.getConstructor(Context::class.java,ViewGroup::class.java).newInstance(context, viewGroup)
        }
    }
    inner class SicknessFactory(val context: Context) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return modelClass.getConstructor(Context::class.java).newInstance(context)
        }
    }

    inner class VerificationViewModelFactory(val context: Context, val viewGroup: ViewGroup, val string:String) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return modelClass.getConstructor(Context::class.java,ViewGroup::class.java,String::class.java).newInstance(context, viewGroup,string)
        }
    }


    inner class BaseViewModelFactoryById(val context: Context, val viewGroup: ViewGroup,val id :Int) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return modelClass.getConstructor(Context::class.java,ViewGroup::class.java,Int::class.java).newInstance(context, viewGroup,id)
        }
    }
    inner class HomeViewModelFactory(val context: Context, val viewGroup: ViewGroup,private val dbHelper: DatabaseHelper) : ViewModelProvider.Factory {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return modelClass.getConstructor(Context::class.java,ViewGroup::class.java,DatabaseHelper::class.java).newInstance(context, viewGroup,dbHelper)

        }
    }


}
