package ir.negarrajabi.viewModel

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.*
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import ir.negarrajabi.model.NewsResponse


/*
  Created by Negar on 2021/07.
 */

class NewsDetailViewModel() : BaseViewModel() {

    lateinit var context: Context;
    lateinit var viewGroup: ViewGroup


    constructor(context: Context, viewGroup: ViewGroup) : this() {
        this.context = context
        this.viewGroup = viewGroup
    }

    fun openLink(view:View){
        val browserIntent =
            Intent(Intent.ACTION_VIEW, Uri.parse(url))
        view.context.startActivity(browserIntent)
    }



    var url: String? = null
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.url)
        }

    var imgUrl: String? = null
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.imgUrl)
        }

    var title: String? = null
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.title)
        }

    var desc: String? = null
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.desc)
        }
 }



