package ir.negarrajabi.application;


import android.app.Application;
import android.content.Context;

import ir.negarrajabi.dagger.component.ApplicationComponent;
import ir.negarrajabi.dagger.component.DaggerApplicationComponent;
import ir.negarrajabi.dagger.module.AppModule;
import ir.negarrajabi.dagger.module.ContextModule;
import ir.negarrajabi.dagger.module.NetModule;
import ir.negarrajabi.dagger.module.PicassoModule;


/*
  Created by Negar on 2021/07.
 */
public class Base extends Application {
    public static int total;
public static Context context;




    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        applicationComponent = DaggerApplicationComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule())
                .contextModule(new ContextModule(this))
                .picassoModule(new PicassoModule())
                .build();


    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

}
