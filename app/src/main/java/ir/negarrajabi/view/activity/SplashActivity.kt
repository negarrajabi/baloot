package ir.negarrajabi.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.view.WindowManager

/*
  Created by Negar on 2021/07.
 */

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        );
        var intent = Intent(this@SplashActivity, MainActivity::class.java)
        startActivity(intent)
        finish()

    }


}



