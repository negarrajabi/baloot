package ir.negarrajabi.view.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.plusAssign
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.telme.view.fragment.navigation.KeepStateNavigator
import ir.negarrajabi.R
import ir.negarrajabi.databinding.ActivityMainBinding

/*
  Created by Negar on 2021/07.
 */

class MainActivity : BaseActivity() {


    private lateinit var binding: ActivityMainBinding
    lateinit var navController: NavController

    companion object {
        lateinit var bottomNavController: BottomNavigationView

    }





    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
//        setSupportActionBar(toolbar)

        navController = findNavController(R.id.nav_host_fragment)


        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment)!!

        // setup custom navigator
        val navigator = KeepStateNavigator(
            this,
            navHostFragment.childFragmentManager,
            R.id.nav_host_fragment
        )
        navController.navigatorProvider += navigator


        navController.setGraph(R.navigation.mobile_navigation)
        binding.content1.bottomNav.setupWithNavController(navController)
        bottomNavController = binding.content1.bottomNav


    }

    fun hideBottomBar(isHidden: Boolean) {
        binding.content1.bottomNav.setVisibility(if (isHidden) View.GONE else View.VISIBLE)
    }





}
