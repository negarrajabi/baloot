package ir.negarrajabi.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import java.util.*


/*
  Created by Negar on 2021/07.
 */

open class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setAppLocale("fa")
    }



    fun setAppLocale(localeCode: String) {
        val locale = Locale(localeCode, "Ir")
        val resources = getResources();
        val dm = resources.getDisplayMetrics();
        val config = resources.getConfiguration();
        // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
        config.setLocale(locale);
        //  } else {
        config.locale = locale;
        //  }
        //  if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
        getApplicationContext().createConfigurationContext(config);
        // } else {
        resources.updateConfiguration(config, dm);
        // }
    }
}