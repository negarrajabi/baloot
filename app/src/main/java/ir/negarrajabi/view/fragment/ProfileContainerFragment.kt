package ir.negarrajabi.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import ir.negarrajabi.databinding.FragmentProfileContainerBinding
import ir.negarrajabi.view.fragment.BaseFragment

/*
  Created by Negar on 2021/07.
 */

class ProfileContainerFragment : BaseFragment() {

    private lateinit var binding: FragmentProfileContainerBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentProfileContainerBinding.inflate(inflater, container, false)
        return binding.root
    }



}
