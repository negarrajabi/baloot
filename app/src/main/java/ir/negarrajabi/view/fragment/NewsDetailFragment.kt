package ir.negarrajabi.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import ir.negarrajabi.R
import ir.negarrajabi.config.preference.PrefKey
import ir.negarrajabi.databinding.FragmentNewsDetailBinding
import ir.negarrajabi.local.Article
import ir.negarrajabi.model.NewsResponse
import ir.negarrajabi.view.activity.MainActivity
import ir.negarrajabi.viewModel.NewsDetailViewModel
import ir.negarrajabi.viewModel.factory.MyViewModelFactory
import kotlinx.android.synthetic.main.fragment_news_detail.view.*


/*
  Created by Negar on 2021/07.
 */

class NewsDetailFragment : BaseFragment() {
    lateinit var binding: FragmentNewsDetailBinding;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_news_detail,
            container,
            false
        )
        val view = binding.getRoot()
        val viewModelFactory = MyViewModelFactory().BaseViewModelFactory(context!!, view.viewGroup)
        val viewModel: NewsDetailViewModel = ViewModelProviders.of(this, viewModelFactory).get(NewsDetailViewModel::class.java)
        val cacheData = arguments!!.getBoolean(PrefKey.TYPE)
        var article = Article("","","","","","","")
        var request = NewsResponse.Article("","","","",NewsResponse.Source("",""),"","","")
        if(cacheData)
            article = arguments!!.get(PrefKey.ARTICLE) as Article
        else
            request = arguments!!.get(PrefKey.NEWS) as NewsResponse.Article

        (activity as MainActivity).hideBottomBar(true)



        binding.viewModel = viewModel

        if(cacheData){
            binding.viewModel!!.title = article.title
            binding.viewModel!!.desc =article.description
            binding.viewModel!!.imgUrl = article.urlToImage
            binding.viewModel!!.url = article.url
        }
        else{
            binding.viewModel!!.title = request.title
            binding.viewModel!!.desc =request.description
            binding.viewModel!!.imgUrl = request.urlToImage
            binding.viewModel!!.url = request.url
        }
        return view
    }

}