package ir.negarrajabi.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetBehavior
import ir.negarrajabi.R
import ir.negarrajabi.databinding.FragmentProfileBinding
import ir.negarrajabi.view.activity.MainActivity
import ir.negarrajabi.viewModel.ProfileViewModel
import ir.negarrajabi.viewModel.factory.MyViewModelFactory
import kotlinx.android.synthetic.main.dialog_about.view.*

/*
  Created by Negar on 2021/07.
 */

class ProfileFragment : BaseFragment() {

    private lateinit var binding: FragmentProfileBinding
    lateinit var bottomSheet: BottomSheetBehavior<ConstraintLayout>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentProfileBinding>(
            inflater,
            R.layout.fragment_profile,
            container,
            false
        )
        val view = binding.root
        bottomSheet = BottomSheetBehavior.from(view.bottom_sheet)

        (activity as MainActivity).hideBottomBar(false)

            val viewModelFactory =
                MyViewModelFactory().BaseViewModelFactory(context!!, binding.viewGroup)
            val viewModel: ProfileViewModel = ViewModelProviders.of(this, viewModelFactory).get(
                ProfileViewModel::class.java
            )
            binding.viewModel = viewModel
            viewModel.bottomSheet = bottomSheet
            setupBottomSheet()

        binding.aboutBtn.setOnClickListener {
            if (bottomSheet.state == BottomSheetBehavior.STATE_COLLAPSED) {
                bottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED)
                (activity as MainActivity).hideBottomBar(true)
                return@setOnClickListener
            }
        }
        binding.tBack.setOnClickListener {
            bottomSheet.state = BottomSheetBehavior.STATE_COLLAPSED
            (activity as MainActivity).hideBottomBar(false)

        }


        return view
    }
    fun setupBottomSheet() {
        bottomSheet.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(@NonNull bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    binding.tBack.visibility = View.VISIBLE
                } else if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    binding.tBack.visibility = View.GONE

                }
            }

            override fun onSlide(@NonNull bottomSheet: View, slideOffset: Float) {}
        })


    }
}