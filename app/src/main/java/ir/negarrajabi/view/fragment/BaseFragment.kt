package ir.negarrajabi.view.fragment

import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.fragment.app.Fragment


/*
  Created by Negar on 2021/07.
 */

open class BaseFragment : Fragment() {

    open fun showKeyboard(editText: EditText) {
        editText.requestFocus()
        val imm =
            editText.context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm!!.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
    }
    open fun closeKeyboard(editText: EditText) {
        val imm =
            activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm!!.hideSoftInputFromWindow(editText.getWindowToken(), 0)
    }
}