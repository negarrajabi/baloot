package ir.negarrajabi.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import ir.negarrajabi.R
import ir.negarrajabi.adapter.ArticleAdapter
import ir.negarrajabi.adapter.NewsAdapter
import ir.negarrajabi.databinding.FragmentHomeBinding
import ir.negarrajabi.local.Article
import ir.negarrajabi.local.DatabaseBuilder
import ir.negarrajabi.local.DatabaseHelperImpl
import ir.negarrajabi.local.Status
import ir.negarrajabi.view.activity.MainActivity
import ir.negarrajabi.viewModel.HomeViewModel
import ir.negarrajabi.viewModel.factory.MyViewModelFactory
import kotlinx.android.synthetic.main.fragment_home.*

/*
  Created by hotmint on 2020-03-25.
 */
class HomeFragment : BaseFragment() {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var viewModel: HomeViewModel
//    private lateinit var adapter: ArticleAdapter
private lateinit var adapter: NewsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentHomeBinding>(
            inflater,
            R.layout.fragment_home,
            container,
            false
        )
        val view = binding.root

        (activity as MainActivity).hideBottomBar(false)


            val viewModelFactory =
                MyViewModelFactory().HomeViewModelFactory(context!!, binding.viewGroup,
                    DatabaseHelperImpl(DatabaseBuilder.getInstance(context!!))
                )
            viewModel = ViewModelProviders.of(this, viewModelFactory).get(
                HomeViewModel::class.java
            )

            binding.viewModel = viewModel
        setupUI()
        setupObserver()
        return view
    }

    private fun setupUI() {
        binding.recyclerViewArticle.layoutManager = LinearLayoutManager(context)
//        adapter =
//            ArticleAdapter(
//                arrayListOf(),viewModel
//            )
        adapter = NewsAdapter(context!!,viewModel.articleT)
        binding.recyclerViewArticle.addItemDecoration(
            DividerItemDecoration(
                binding.recyclerViewArticle.context,
                (binding.recyclerViewArticle.layoutManager as LinearLayoutManager).orientation
            )
        )
        binding.recyclerViewArticle.adapter = adapter
    }

    private fun setupObserver() {
        viewModel.getUsers().observe(context as LifecycleOwner, Observer  {
            when (it.status) {
                Status.SUCCESS -> {
                    it.data?.let { users -> renderList(users) }
                    binding.recyclerViewArticle.visibility = View.VISIBLE
                }
                Status.LOADING -> {
                    binding.recyclerViewArticle.visibility = View.GONE
                }
                Status.ERROR -> {
                    Toast.makeText(context, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun renderList(users: List<Article>) {
//        adapter.addData(users)
        adapter.notifyDataSetChanged()
    }


}