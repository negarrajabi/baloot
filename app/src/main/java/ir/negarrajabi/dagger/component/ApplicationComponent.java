package ir.negarrajabi.dagger.component;

/*
  Created by Negar on 2021/07.
 */

import android.app.Application;

import com.google.gson.Gson;

import ir.negarrajabi.application.Base;
import ir.negarrajabi.dagger.module.AppModule;
import ir.negarrajabi.dagger.module.NetModule;
import ir.negarrajabi.dagger.module.PicassoModule;

import com.parhun.remote.BaseRepository;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;


@Singleton
@Component(modules = {AppModule.class, NetModule.class, PicassoModule.class})
public interface ApplicationComponent {

    void inject(Base app);
    void inject(BaseRepository rep);
    Application application();

}
