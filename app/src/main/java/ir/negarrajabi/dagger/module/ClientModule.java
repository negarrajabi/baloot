package ir.negarrajabi.dagger.module;

import android.content.Context;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;


/*
  Created by Negar on 2021/07.
 */

@Module(includes = ContextModule.class)
public class ClientModule {
  @Provides
  public OkHttpClient client(HttpLoggingInterceptor interceptor, final Context context) {
    return new OkHttpClient.Builder()
      .addInterceptor(interceptor)
      .addInterceptor(chain -> {
        Request.Builder request = chain.request().newBuilder()
          .addHeader("Accept", "application/json")
          .addHeader("Content-Type", "application/json");

        return chain.proceed(request.build());
      })
      .readTimeout(30, TimeUnit.SECONDS)
      .build();
  }

  @Singleton
  @Provides
  public HttpLoggingInterceptor interceptor() {
    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    return interceptor;
  }
}
