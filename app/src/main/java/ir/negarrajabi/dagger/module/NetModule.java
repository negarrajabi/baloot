package ir.negarrajabi.dagger.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static ir.negarrajabi.config.AppData.BASE_URL;


/*
  Created by Negar on 2021/07.
 */

@Module(includes = ClientModule.class)
public class NetModule {
  @Singleton
  @Provides
  public Retrofit retrofit( OkHttpClient client, Gson gson) {
    return new Retrofit.Builder()
      .baseUrl(BASE_URL)
      .client(client)
      .addConverterFactory(GsonConverterFactory.create(gson))
      .build();
  }

  @Singleton
  @Provides
  public Gson gson() {
    return new GsonBuilder()
      .setDateFormat("yyyy-MM-dd' 'HH:mm:ss")
      .create();
  }
}
