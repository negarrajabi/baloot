package ir.negarrajabi.remote.repository

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.parhun.remote.BaseRepository
import ir.negarrajabi.R
import ir.negarrajabi.config.AppData
import ir.negarrajabi.model.*
import ir.negarrajabi.remote.ApiService
import ir.negarrajabi.viewModel.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/*
  Created by Negar on 2021/07.
 */

class NewsRepository(context: Context) : BaseRepository(context) {


    val newsLiveData = MutableLiveData<ArrayList<HomeViewModel>>()
    val errorLiveData = MutableLiveData<ErrorApi>()


    fun getNews(page: Int) {
        val call = retrofit.create(ApiService::class.java)
            .getNews("tesla","publishedAt",AppData.API_KEY,page)
        call.enqueue(object : Callback<NewsResponse> {
            override fun onResponse(
                call: Call<NewsResponse>, response: Response<NewsResponse>
            ) {
                if (response.isSuccessful) {
                    val list = ArrayList<HomeViewModel>()
                    response.body()!!.articles.forEach {
                        list.add(HomeViewModel(it))
                    }

                    newsLiveData.setValue(list)
                } else {
                    try {
                        val errorApi = ErrorApi(
                            ErrorApi.NOT_SUCCESS,
                            context.getString(R.string.not_success_api)
                        )
                        errorLiveData.setValue(errorApi)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<NewsResponse>, t: Throwable) {
                val errorApi = ErrorApi(ErrorApi.ON_FAILURE_ERROR)
                errorLiveData.setValue(errorApi)
            }
        })
    }


}