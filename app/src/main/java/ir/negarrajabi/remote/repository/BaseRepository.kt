package com.parhun.remote

import android.content.Context
import com.google.gson.Gson
import ir.negarrajabi.application.Base
import retrofit2.Retrofit
import javax.inject.Inject

/*
  Created by Negar on 2021/07.
 */

open class BaseRepository(var context: Context) {

    @Inject
    lateinit var retrofit: Retrofit
    @Inject
    lateinit var gson: Gson
    init {

        (context.applicationContext as Base).applicationComponent.inject(this)

    }
}