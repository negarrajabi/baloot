package ir.negarrajabi.remote


import ir.negarrajabi.model.*
import retrofit2.Call
import retrofit2.http.*

/*
  Created by Negar on 2021/07.
 */

interface ApiService {

    @GET("/v2/everything")
    fun getNews(@Query("q") q: String,
                @Query("sortBy") sortBy: String,
                @Query("apiKey") apiKey: String,
                @Query("page") page: Int): Call<NewsResponse>

}